import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User, Role } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userSubject$ = new BehaviorSubject<User | null>(null);

  public get user$() {
    return this.userSubject$.asObservable();
  }

  constructor() {
    this.login(); // zeby od razu byl zalogowany
  }

  login() {
    // login buisnes logic
    this.userSubject$.next({
      id: 1,
      role: Role.ADMIN,
      name: 'Piotr'
    });
  }

  logout() {
    this.userSubject$.next(null);
  }
}
