import { Directive, TemplateRef, ViewContainerRef, Input, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../services/user.service';
import { User, Role } from '../../models';
import { BehaviorSubject, combineLatest, Observable, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective implements OnInit, OnDestroy {

  private hasView = false;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private userService: UserService
  ) { }

  private role$ = new BehaviorSubject<Role>(Role.GUEST);
  private destroy$ = new Subject();

  @Input()
  set appHasRole(role: Role) {
    this.role$.next(role);
  }
  private sub: Subscription;

  ngOnInit() {

    const condition$: Observable<any> = combineLatest(
      this.userService.user$,
      this.role$,
      (user, role) => {
        if (!user) { return false; }
        return user.role === role;
      }
    );

    this.sub = condition$
      .pipe(takeUntil(this.destroy$))
      .subscribe((condition: boolean) => {
        console.log('my user', condition);
        if (condition && !this.hasView) {
          this.viewContainer.createEmbeddedView(this.templateRef);
          this.hasView = true;
        } else if (!condition && this.hasView) {
          this.viewContainer.clear();
          this.hasView = false;
        }
      });

  }

  ngOnDestroy() {
    if(this.sub) {
      this.sub.unsubscribe();
    }
    // this.destroy$.next();
    // this.destroy$.complete();
  }
}


