import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './shared/components/home/home.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent,
}, {
  path: 'videos',
  loadChildren: () => import(`./videos/videos.module`).then(module => module.VideoModule)
}, {
  path: 'music',
  loadChildren: () => import(`./music/music.module`).then(module => module.MusicModule)
}];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    {
      // preloadingStrategy: PreloadAllModules,

    }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }

// const items = [
//   'item',
//   'dwa',
//   'ff',
//   'dd',
//   'ddd',
// ];
