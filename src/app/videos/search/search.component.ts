import { Component, OnInit, Inject } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { share, catchError, map, switchMap } from 'rxjs/operators';
import { Song, ResponseError } from '../../models';
import { API_DOMAIN } from '../../tokens';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public query = '';

  public results$: Observable<Song[]>;

  constructor(
    private http: HttpClient,
    @Inject(API_DOMAIN) private apiDomain: string
  ) { }

  ngOnInit() {

  }

  onSubmit(event) {
    console.log('SEARCH', this.query);

    const url = this.apiDomain + '/songs?_expand=artist&q=' + this.query;

    this.results$ = this.http.get<Song[]>(url).pipe(
      // share()
      // switchMap((res) => {
      //   if((res as any).error) {
      //     return throwError('404');
      //   }
      //   return of(res);
      // }),
      catchError(err => {
        return of([]);
      })
    );
  }

}

const comp: Partial<SearchComponent> = {
  query: null,
  ngOnInit() {}
};
