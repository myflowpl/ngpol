import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_DOMAIN } from '../../tokens';
import { Observable, BehaviorSubject } from 'rxjs';
import { Artist } from '../../models';
import { tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  artists$ = new BehaviorSubject<Artist[]>([]);

  constructor(
    private http: HttpClient,
    @Inject(API_DOMAIN) private baseUrl: string
  ) { }

  public getArtistById(id: number): Observable<Artist> {
    return this.http.get<Artist>(this.baseUrl + '/artists/' + id);
  }

  public loadArtists(): Observable<Artist[]> {
    return this.http.get<Artist[]>(this.baseUrl + '/artists').pipe(
      tap(artists => this.artists$.next(artists))
    );
  }

  public updateArtist(data: Artist): Observable<Artist> {
    return this.http.patch<Artist>(this.baseUrl + '/artists/' + data.id, data).pipe(
      tap((artist) => {
        const artists = this.artists$.getValue().map(a => {
          if (a.id === artist.id) {
            a.name = artist.name
            // return a;
            return {
              ...a,
              ...artist
            }
            // return artist;
          } else {
            return a;
          }
        });
        this.artists$.next(artists);
      })
    );
  }
}

// const a = {foo: 'jeden', ble: 'xxx'}
// const b = {bar: 'dwa', foo: 'jeden plus'}

// const c = Object.assign({}, a, b);
// const d = {
//   ...a,
//   name: 'Pawel'
// }

// const g = [
//   ...arr1,
//   ...arr2
// ]
