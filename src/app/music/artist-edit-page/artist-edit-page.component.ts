import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArtistService } from '../services/artist.service';
import { tap, switchMap, catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-artist-edit-page',
  templateUrl: './artist-edit-page.component.html',
  styleUrls: ['./artist-edit-page.component.scss']
})
export class ArtistEditPageComponent implements OnInit {

  error: any;

  artist$; // : Observable<number>;

  constructor(
    private route: ActivatedRoute,
    private artistService: ArtistService,
    private fb: FormBuilder
  ) { }

  artistForm = this.fb.group({
    id: ['', Validators.required],
    name: ['', Validators.required],
    img: ['', Validators.required],
    location: this.fb.group({
      lat: ['', Validators.required],
      lng: ['', Validators.required],
    })
  });

  ngOnInit() {
    this.artist$ = this.route.params.pipe(
      map(params => params.id),
      tap(() => this.error = null),
      switchMap(id =>
        this.artistService.getArtistById(id).pipe(
          catchError(err => {
            this.error = err;
            return of(null);
          })
        )
      ),
      tap(artist => this.artistForm.patchValue(artist))
    );
  }

  onSubmit() {
    console.log('SUBMIT', this.artistForm.value);
    this.artistService.updateArtist(this.artistForm.value).subscribe();
  }

}
