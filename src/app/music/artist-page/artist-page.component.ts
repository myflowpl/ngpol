import { Component, OnInit } from '@angular/core';
import { ArtistService } from '../services/artist.service';
import { Observable } from 'rxjs';
import { Artist } from '../../models';

@Component({
  selector: 'app-artist-page',
  templateUrl: './artist-page.component.html',
  styleUrls: ['./artist-page.component.scss']
})
export class ArtistPageComponent implements OnInit {

  artists$: Observable<Artist[]>;

  constructor(
    private artistService: ArtistService
  ) { }

  ngOnInit() {
    this.artists$ = this.artistService.artists$;
    this.artistService.loadArtists().subscribe();
  }

}
