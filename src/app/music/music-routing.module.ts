import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArtistPageComponent } from './artist-page/artist-page.component';
import { ArtistViewPageComponent } from './artist-view-page/artist-view-page.component';
import { ArtistEditPageComponent } from './artist-edit-page/artist-edit-page.component';


const routes: Routes = [{
  path: '',
  redirectTo: 'artists'
}, {
  path: 'artists',
  component: ArtistPageComponent,
  children: [{
    path: 'view/:id',
    component: ArtistViewPageComponent,
    data: {
      isView: true
    }
  }, {
    path: 'edit/:id',
    component: ArtistEditPageComponent,
    data: {
      isEdit: true
    }
  }, {
    path: 'add/:id',
    component: ArtistEditPageComponent,
    data: {
      isEdit: false
    }
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }
