import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { ArtistService } from '../services/artist.service';
import { UserService } from '../../user/services/user.service';

@Component({
  selector: 'app-artist-view-page',
  templateUrl: './artist-view-page.component.html',
  styleUrls: ['./artist-view-page.component.scss'],
  // providers: [UserService]
})
export class ArtistViewPageComponent implements OnInit {

  error: any;

  artist$; // : Observable<number>;

  constructor(
    private route: ActivatedRoute,
    private artistService: ArtistService
  ) { }

  ngOnInit() {

    console.log('DATA', this.route.snapshot.data)
    console.log('id', this.route.snapshot.params.id)

    this.artist$ = this.route.params.pipe(
      map(params => params.id),
      tap(() => this.error = null),
      switchMap(id =>
        this.artistService.getArtistById(id).pipe(
          catchError(err => {
            this.error = err;
            return of(null);
          })
        )
      ),
      map(artist => artist)
    );
  }

}
