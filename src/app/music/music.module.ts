import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicRoutingModule } from './music-routing.module';
import { ArtistPageComponent } from './artist-page/artist-page.component';
import { ArtistViewPageComponent } from './artist-view-page/artist-view-page.component';
import { ArtistEditPageComponent } from './artist-edit-page/artist-edit-page.component';
import { ArtistProfileComponent } from './components/artist-profile/artist-profile.component';
import { UserModule } from '../user/user.module';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ArtistPageComponent,
    ArtistViewPageComponent,
    ArtistEditPageComponent,
    ArtistProfileComponent
  ],
  imports: [
    CommonModule,
    MusicRoutingModule,
    UserModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class MusicModule { }
