import { Song } from './';

export class Coords {
  lat: number;
  lng: number;
}
export interface Artist {
  id: number;
  name: string;
  img: string;
  location?: Coords;
}
export interface Playlist {
  id: number;
  name: string;
}

export interface PlaylistSongs {
  id: number;
  playlistId: number;
  songId: number;
  song?: Song;
  playlist?: Playlist;
}


export interface User {
  id: number;
  name: string;
  password?: string;
  role?: Role;
}

export interface ResponseError {
  error: boolean;
  message: string;
}

export enum Role {
  ADMIN = 'admin',
  USER = 'user',
  GUEST = 'guest'
}
