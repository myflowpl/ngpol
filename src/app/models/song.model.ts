
export class Song {
  id: number;
  title: string;
  year: string;
  artistId: number;
  webUrl: string;
  genders?: string[];
  favorite?: boolean;
}
