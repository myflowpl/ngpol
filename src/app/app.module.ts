import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';
import { API_DOMAIN, IMAGES_DOMAIN } from './tokens';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [{
    provide: API_DOMAIN,
    useValue: window['pol_conf'].API_DOMAIN
  },{
    provide: IMAGES_DOMAIN,
    useValue: 'http://www.songnotes.cc/images/artists/'
  }
  // {
  //   provide: APP_INITIALIZER,
  //   useFactory: async (appInitService) => {

  //   },
  //   deps: [AppInitService],
  //   multi: true
  // }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
