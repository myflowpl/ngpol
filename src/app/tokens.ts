import { InjectionToken } from '@angular/core';

export const API_DOMAIN = new InjectionToken('API_DOMAIN');
export const IMAGES_DOMAIN = new InjectionToken('IMAGES_DOMAIN');
