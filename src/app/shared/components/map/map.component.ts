

import * as L from 'leaflet';
L.Icon.Default.imagePath = '/assets/leaflet/images/';
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, OnChanges, SimpleChange, Input } from '@angular/core';
import { ReplaySubject } from 'rxjs';


export interface Coords {
  lat: number;
  lng: number;
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnChanges {

  @ViewChild('mapContainer', { static: true }) mapContainer: ElementRef;

  @Output() mapClick = new EventEmitter<Coords>();

  @Input() coords: Coords;

  private coords$ = new ReplaySubject<Coords>(1);

  constructor() { }

  marker: L.Marker;

  ngOnInit() {

    const map = L.map(this.mapContainer.nativeElement).setView([51.505, -0.09], 13);

    L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png`', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    })
      .addTo(map);

    map.on('click', (e: L.LeafletMouseEvent) => {
      this.mapClick.next(e.latlng);
    });

    this.coords$.subscribe((coords: Coords) => {

      if (this.marker) {
        map.removeLayer(this.marker);
      }
      this.marker = L.marker(coords).addTo(map);
      map.panTo(coords);

    });


  }

  ngOnChanges(changes: { [key: string]: SimpleChange }) {
    console.log('CHANGE', changes);
    if (changes.hasOwnProperty('coords') && changes.coords.currentValue) {
      this.coords$.next(changes.coords.currentValue);
    }
  }
}
