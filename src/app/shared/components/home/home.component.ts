import { Component, OnInit } from '@angular/core';
import { Coords } from '../map/map.component';

export interface IconSrc {
  id: number;
  src: string;
}
@Component({
  // selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title = 'Welcome to Polkomtel :)';

  day = 2;
  visible = false;

  public items: IconSrc[] = [
    {id: 1, src: 'jeden.png'},
    {id: 2, src: 'dwa.png'},
  ];


  coordsFromHome: Coords;

  constructor() { }

  ngOnInit() {
  }

  getSubtitle() {
    return 'Szkolenie z Angulara';
  }

  onInputChange(e) {
    this.day = parseInt(e.target.value, 10);

    console.log('on change', e);
  }

  onIconChange(e: Date) {
    console.log('onIconChange', e);
  }

}
