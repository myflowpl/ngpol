import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent implements OnInit, OnDestroy {

  @Input()
  iconSrc: string;

  @Output()
  iconChange = new EventEmitter<string>();

  constructor() { }

  onIconBtnClick() {
    this.iconChange.emit('event data from click');
  }
  ngOnInit() {
    console.log('ICON INIT', this.iconSrc);

    // this.iconChange.emit('event data');

  }

  ngOnDestroy(): void {
    console.log('ICON DESTROY');
  }

}
