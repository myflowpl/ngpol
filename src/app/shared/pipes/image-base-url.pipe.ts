import { Pipe, PipeTransform, Inject } from '@angular/core';
import { IMAGES_DOMAIN } from '../../tokens';

@Pipe({
  name: 'imageBaseUrl'
})
export class ImageBaseUrlPipe implements PipeTransform {

  constructor(@Inject(IMAGES_DOMAIN) private baseUrl: string) {}

  transform(value: any, ...args: any[]): any {
    return this.baseUrl + value;
  }

}
