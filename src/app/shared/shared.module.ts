import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './components/menu/menu.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { IconComponent } from './components/icon/icon.component';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';

import { AppRoutingModule } from '../app-routing.module';
import { RouterModule } from '@angular/router';
import { MapComponent } from './components/map/map.component';
import { ImageBaseUrlPipe } from './pipes/image-base-url.pipe';

@NgModule({
  declarations: [
    MenuComponent,
    FooterComponent,
    HomeComponent,
    IconComponent,
    MapComponent,
    ImageBaseUrlPipe
  ],
  imports: [
    CommonModule,
    MatMenuModule,
    MatButtonModule,
    RouterModule,
    MatInputModule
  ],
  exports: [
    MenuComponent,
    FooterComponent,
    MatMenuModule,
    MatButtonModule,
    MatInputModule,
    ImageBaseUrlPipe,
    MapComponent
  ]
})
export class SharedModule { }
